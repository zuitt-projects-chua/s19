//alert("hi")

let string1 = "Zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "teaches";
let string5 = "Javacript";

//template literals ``-backticks , ${}-placeholder

console.log(string1 + " " + string2 + " " +  string3 + " " +  string4 + " " +  string5 );
console.log(Math.pow(5,3));

let sentence = `${string1}...`


/*
Ecmascript or ES6
ES6 new version of javascript
Javascript formally known as ecmascript
*/

let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);

let sentence2 = `The result of five to the power of 2 is ${5**2}`
console.log(sentence2);

//Array Destructuring - to save array items in a variable

let array = ["Kobe", "Lebron", "Shaq", "Westbrook"];
console.log(array[2])

let lakerPlayer1 = array[3];
let lakerPlayer2 = array[0];

//const [westbrook] = array;
//console.log(westbrook); westbrrok take svalue kobe
//const [, , ,westbrook]

//order must be observed in array
const [kobe, lebron, shaq] = array; //<arrayName> refering to
//used to easily assigning item to variable
console.log(kobe);
console.log(lebron);
console.log(shaq);

console.log(array);

let array2 = ["Curry", "Lillard", "Paul", "Irving"];

const [pointGuard1,  pointGuard2, pointGuard3, pointGuard4] = array2;

console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

let bandmembers = ["Hayley", "Zac", "Jeremy", "Taylor"];

const [vocals, lead, ,bass] = bandmembers;

console.log(vocals);
console.log(lead);
console.log(bass);

/*
Object Destructuring
allow us to add the values of object properry into a variable
*/

let person = {
	name: "Jeremy Davis",
	birthday: "September 12, 1989",
	age: 32
};

let sentence3 = `Hi I am ${person.name}`;
console.log(sentence3);

//regardless of order, and existing properrty must be use in const declaration
const {age, firstName, birthday} = person;
console.log(age);
console.log(name);
console.log(birthday);

let pokemon1 = {
	pname: "Charmander",
	level: 11,
	type: "Fire",
	moves: ["Ember", "Scratch", "Leer"]
};

const {pname, level, type, moves} =pokemon1;

let sentenceNew = `My pokemon is ${pname}, it is in level ${level}. It is a ${type} type. It's moves are ${moves}`;

console.log(sentenceNew);

/*
Arrow functions
*/

//traditional 

function displayMsg (){
	console.log("Hello World");
}

//arrow function

const hello = () => {
	console.log("Hello World")
}

//invoking are the same
displayMsg();
hello();


//the parameter person is new, empty container(parameter)
const greet = (person) => {
	//console.log(`Hi ${person.type}`)
	console.log(`Hi ${person}`)
}

//greet(pokemon1)
greet("Charles");
//arrow function returning value
const addNum = (num1, num2) => num1 + num2
let sum = addNum(55,60);
console.log(sum);

//this keyword

let protagonist = {
	name: "Cloud Strife",
	occupation: "SOLDIER",
	greet: function() {
		//traditional method would have "this" keyword to refer to parent object
		console.log(this);
		console.log(`Hi I am ${this.name}`)
	},
	introduce: () => {
		//arrow function refers to => global window
		console.log(this);
		console.log(`I work as ${this.occupation}`)
	}
}

protagonist.greet();
protagonist.introduce();

/*
Class-Base Objects Blueprints
in javascript,classes are templates of objects
templates-can be used multiple times

*/

//constructor function - more like to help developers determine a function is an object
function Pokemon (name, type, lvl){
	this.name = name;
	this.type = type;
	this.lvl = lvl;
};

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}


let car2 = new Car("Toyota", "Vios", 2002)
console.log(car2);
let car1 = new Car("Cooper", "Mini", 2002)
console.log(car1);








