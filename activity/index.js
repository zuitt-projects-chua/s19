//alert("Hi")

// PArt 1 Debugging Codes

let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101,", " Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401,", " Natural Sciences 402"]
}

function introduce(student){
	
	const {name, birthday, age, isEnrolled, classes} = student;

	this.name = name;
	this.birthday = birthday;
	this.age = age;
	this.isEnrolled = isEnrolled;
	this.classes = classes;

	//Note: You can destructure objects inside functions.

	console.log(`Hi! I'm ${this.name}. I am ${this.age} years old.`);
	console.log(`I study the following courses: ${this.classes}.`);


	/*console.log("Hi! " + "I'm " + student.name + " ." + " I am " + student.age + " years old.");
	console.log("I study the following courses" + student.classes);*/
}

introduce(student1); //display output


//Cube a number

const getCube = (num) => num**3;
let cube = getCube(3);
console.log(cube)



let numArr = [15,16,32,21,21,2]

//Output Array Element block

numArr.forEach(num => console.log(num));



//Output Array element squared number

const numSquared = numArr.map (num  => num**2)


console.log(numSquared);


//Part 2 Classes

//Creation
class dog {
	constructor(name, breed, age){
		this.name = name;
		this.breed = breed;
		this.age = age*7;
	}
}


//Output new objects

let dog1 = new dog("Kimpee", "American Bully", 1)
console.log(dog1);
let dog2 = new dog("Wine", "Chow Chow", 1)
console.log(dog2);





